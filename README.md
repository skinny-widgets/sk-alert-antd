# Skinny Widgets Alert for Antd Theme


alert element

```
npm i sk-alert sk-alert-antd --save
```

then add the following to your html

```html
<sk-config
    theme="antd"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd"
></sk-config>
<sk-alert type="error">Error ! Error !</sk-alert>
<script type="module">
    import { SkAlert } from './node_modules/sk-alert/index.js';

    customElements.define('sk-alert', SkAlert);
</script>
```

#### slots

**default (not specified)** - alert contents

#### attributes

**closable** - close button

#### template

id: SkAlertTpl