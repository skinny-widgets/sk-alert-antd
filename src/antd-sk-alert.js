
import { SkAlertImpl }  from '../../sk-alert/src/impl/sk-alert-impl.js';

export class AntdSkAlert extends SkAlertImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'alert';
    }

    get containerEl() {
        return this.comp.el.querySelector('.ant-alert');
    }

    get closeBtnEl() {
        return this.comp.el.querySelector('.ant-alert-close-icon');
    }

    bindEvents() {
        super.bindEvents();
    }

    unbindEvents() {
        super.unbindEvents();
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
        if (this.comp.type === 'error') {
            this.containerEl.classList.add('ant-alert-error');
        } else if (this.comp.type === 'info') {
            this.containerEl.classList.add('ant-alert-info');
        } else if (this.comp.type === 'warning') {
            this.containerEl.classList.add('ant-alert-warning');
        } else {
            this.containerEl.classList.add('ant-alert-success');
        }
    }
}
